# group horas_proyectosJSG


## Descripción

El grupo de proyectos horas_proyectosJSG lo componen tres proyectos principales, los cuales ayudan a llevar un control de las horas que dedica un empleado a un proyecto en concreto.

## Características principales

-Visualizar proyectos, empleados o registros guardados en la base de datos.
-Crear nuevos proyectos, empleados o registros de horas dedicadas.
-Modificar proyectos, empleados o registros.
-Eliminar proyectos, empleados o registros.

## Estructura

El grupo consta de las siguientes partes:

-api_horas_proyectos
Se trata de una API modelo vista controlador JDBC, desarrollada en Java con Spring boot, se conecta a una base de datos PostgreSQL

-javafx_horas_proyectos
Aplicación de escritorio desarrollada en Java utilizando JavaFX, la cual realiza peticiones a api_horas_proyectos

-app_ios_horas_proyectos
Aplicación móvil desarrollada en Swift para dispositivos iOS, realiza peticiones a api_horas_proyectos. Utiliza SwiftUI.

## Objetivos

Los principales objetivos de este proyecto son:

-Practicar y familiarizarse con los lenguajes de programación Swift y Java, así como con las herramientas SwiftUI, JavaFX y Spring boot.
-Investigar y comprender el funcionamiento de las aplicaciones móviles en iOS.
-Aprender a implementar las operaciones CRUD en las distintas plataformas.

## Licencia

Este proyecto está protegido por derechos de autor. Todos los derechos están reservados. No se concede permiso para copiar, modificar o distribuir el código sin el consentimiento explícito del autor.

Si deseas utilizar este proyecto, por favor, ponte en contacto con el autor para obtener su autorización.

## Contribuciones

En este momento no se admiten contribuciones.

## Autor
José Sánchez García
