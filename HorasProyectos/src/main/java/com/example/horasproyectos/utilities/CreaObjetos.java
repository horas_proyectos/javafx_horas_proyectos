package com.example.horasproyectos.utilities;

import com.example.horasproyectos.models.Empleado;
import com.example.horasproyectos.models.Participan;
import com.example.horasproyectos.models.Proyecto;
import org.json.JSONObject;

public class CreaObjetos {

    /**Este método devuelve un objeto proyecto, según si le mandas un json o un proyecto, aunque el resultado es el mismo*/
    public static Proyecto creaProyectos(JSONObject jsonObject, Proyecto proyecto){

        Proyecto proyectoDevuelto = null;

        if (jsonObject != null) {
            proyectoDevuelto = new Proyecto();
            proyectoDevuelto.setProyectoid(jsonObject.getInt("proyectoid"));
            proyectoDevuelto.setNombre(jsonObject.getString("nombre"));
            proyectoDevuelto.setDescripcion(jsonObject.getString("descripcion"));

            return proyectoDevuelto;
        }

        if (proyecto != null) {
            proyectoDevuelto = new Proyecto();
            proyectoDevuelto.setProyectoid(proyecto.getProyectoid());
            proyectoDevuelto.setNombre(proyecto.getNombre());
            proyectoDevuelto.setDescripcion(proyecto.getDescripcion());

            return proyectoDevuelto;
        }

        return null;
    }


    /**Este método devuelve un objeto empleado*/
    public static Empleado creaEmpleados(JSONObject jsonObject, Empleado empleado){

        if (jsonObject != null) {
            Empleado empleadoDevuelto = new Empleado();
            empleadoDevuelto.setEmpleadoid(jsonObject.getInt("empleadoid"));
            empleadoDevuelto.setNombreempleado(jsonObject.getString("nombreempleado"));
            empleadoDevuelto.setTelefono(jsonObject.getString("telefono"));

            return empleadoDevuelto;
        }

        if (empleado != null) {
            Empleado empleadoDevuelto = new Empleado();
            empleadoDevuelto.setEmpleadoid(empleado.getEmpleadoid());
            empleadoDevuelto.setNombreempleado(empleado.getNombreempleado());
            empleadoDevuelto.setTelefono(empleado.getTelefono());

            return empleadoDevuelto;
        }

        return null;
    }


    /**Este método devuelve un objeto participan con su objeto empleado y proyecto correspondiente asociado si le llega un json, si recibe
     * un objeto participan devuelve otro igual pero sin el objeto empleado y proyecto asociados, ya que no es necesario*/
    public static Participan creaParticipan(JSONObject jsonObject, Participan participan) {

        Participan participanDevuelto;

        if (jsonObject != null) {
            Integer registroId = jsonObject.getInt("registroid");

            participanDevuelto = new Participan();
            participanDevuelto.setRegistroid(jsonObject.getInt("registroid"));
            participanDevuelto.setEmpleadoid(jsonObject.getInt("empleadoid"));
            participanDevuelto.setProyectoid(jsonObject.getInt("proyectoid"));
            participanDevuelto.setHoras(jsonObject.getFloat("horas"));

            //Seguro que hay formas más sencillas de hacerlo, pero ya que tengo hecho el withAll quería utilizarlo
            String resultStream = BridgeAPI.conection(Consultas.CONSULTA_LISTAR_PARTICIPAN + registroId + Consultas.COMPLETA_CONSULTA_WITH_ALL);

            //Repaso: Los [] es un Array de Json, y los {} objetos Json, solo hay que mirar el resultstream y usar lo que necesites
            JSONObject jsonObjectWithAll = new JSONObject(resultStream);
            JSONObject jsonEmpleado = jsonObjectWithAll.getJSONObject("empleado");
            JSONObject jsonProyecto = jsonObjectWithAll.getJSONObject("proyecto");

            Empleado empleado = creaEmpleados(jsonEmpleado, null);
            Proyecto proyecto = creaProyectos(jsonProyecto, null);

            participanDevuelto.setEmpleado(empleado);
            participanDevuelto.setProyecto(proyecto);

            return participanDevuelto;
        }

        if (participan != null) {

            participanDevuelto = new Participan();
            participanDevuelto.setRegistroid(participan.getRegistroid());
            participanDevuelto.setEmpleadoid(participan.getEmpleadoid());
            participanDevuelto.setProyectoid(participan.getProyectoid());
            participanDevuelto.setHoras(participan.getHoras());

            return participanDevuelto;
        }

        return null;
    }
}
