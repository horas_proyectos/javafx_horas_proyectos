package com.example.horasproyectos.models;
import java.util.Objects;

public class Empleado {


    private Integer empleadoid;
    private String nombreempleado;
    private String telefono;

    //private List<Participan> participanList = new ArrayList<>();

    public Integer getEmpleadoid() {
        return empleadoid;
    }

    public void setEmpleadoid(Integer empleadoid) {
        this.empleadoid = empleadoid;
    }

    public String getNombreempleado() {
        return nombreempleado;
    }

    public void setNombreempleado(String nombreempleado) {
        this.nombreempleado = nombreempleado;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Empleado() {
    }

    public void setEmpleadoid(int empleadoid) {
        this.empleadoid = empleadoid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Empleado)) return false;
        Empleado empleado = (Empleado) o;
        return empleadoid == empleado.empleadoid;
    }

    @Override
    public int hashCode() {
        return Objects.hash(empleadoid);
    }

    @Override
    public String toString() {
        return nombreempleado;
    }
}
