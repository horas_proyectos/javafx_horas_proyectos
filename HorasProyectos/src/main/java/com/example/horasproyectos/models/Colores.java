package com.example.horasproyectos.models;

import javax.swing.text.html.ImageView;

public class Colores {

    private ImageView imageView;
    private String color;
    private String nombre;

    public Colores(ImageView imageView, String color, String nombre) {
        this.imageView = imageView;
        this.color = color;
        this.nombre = nombre;
    }

    public Colores(String color, String nombre) {
        this.color = color;
        this.nombre = nombre;
    }

    public Colores() {
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return nombre;
    }
}
