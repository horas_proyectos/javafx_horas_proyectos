package com.example.horasproyectos.exceptions;

public class NombreNoValido extends Exception{

    public NombreNoValido(){
        super("El nombre introducido no es válido.\nMínimo debe contener 3 letras");
    }
}