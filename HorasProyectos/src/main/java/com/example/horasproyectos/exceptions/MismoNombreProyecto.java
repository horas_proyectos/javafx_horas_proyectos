package com.example.horasproyectos.exceptions;

public class MismoNombreProyecto extends Exception{

    public MismoNombreProyecto(){
        super("Error: el nombre de ese registro ya existe.\nPor favor, introduzca un nombre de proyecto distinto");
    }
}