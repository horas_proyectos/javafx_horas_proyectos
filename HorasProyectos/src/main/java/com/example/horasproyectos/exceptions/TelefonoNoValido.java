package com.example.horasproyectos.exceptions;

public class TelefonoNoValido extends Exception{

    public TelefonoNoValido(){
        super("El número de teléfono no es válido.\nPor favor, introduzca otro número.");
    }
}