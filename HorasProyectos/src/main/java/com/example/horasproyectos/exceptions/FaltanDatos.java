package com.example.horasproyectos.exceptions;

import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class FaltanDatos extends Exception{

    public FaltanDatos(TextField tfHorasNombreEmpleado, TextField tfHorasNombreProyecto){
            super("Faltan datos necesarios para incurrir las horas.\nPor favor, seleccione un elemento de cada tabla");
    }

    public FaltanDatos(TextField tfHorasNombreEmpleado){
        super("Nombre del empleado requerido.\nPor favor, seleccione un empleado de la tabla");
    }

    public FaltanDatos(TextArea taDescripNewProject){
        super("Descripción del proyecto requerida.\nPor favor, complete este campo");
    }

    public FaltanDatos(){
        super("Nombre del proyecto requerido.\nPor favor, seleccione un proyecto de la tabla");
    }

    public FaltanDatos(String sinSeleccionarParticipanTabla){
        super("Datos requeridos.\nPor favor, seleccione un elemento de la tabla de \"Horas incurridas\"");
    }

    public FaltanDatos(String sinSeleccionarEmpleadoOproyectoTabla, String adorno){
        super("Datos requeridos.\nPor favor, seleccione un elemento de la tabla de Empleados o Proyectos");
    }
}