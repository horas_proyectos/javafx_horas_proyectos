package com.example.horasproyectos.exceptions;

import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class SuperadoLimiteDeBd  extends Exception{

    public SuperadoLimiteDeBd(TextArea textAreaDescript){
        super("Error: Superado el límite de la BD.\nPor favor, reduzca el tamaño de la descripción.");
    }

    public SuperadoLimiteDeBd(TextField textFieldNombreProyecto, TextArea textAreaDescript){
        super("Error: Superado el límite de la BD.\nPor favor, reduzca el tamaño del nombre del proyecto.");
    }

    public SuperadoLimiteDeBd(TextField textFieldNombreEmpleado){
        super("Error: Superado el límite de la BD.\nPor favor, reduzca el tamaño del nombre del empleado.");
    }
}