package com.example.horasproyectos.exceptions;

public class UpdateSinCambios extends Exception {

    public UpdateSinCambios() {
        super("Ha introducido los mismos datos.\nNo se han producido cambios en el registro");
    }
}