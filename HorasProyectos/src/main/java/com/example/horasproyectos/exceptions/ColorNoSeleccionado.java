package com.example.horasproyectos.exceptions;

public class ColorNoSeleccionado extends Exception{

    public ColorNoSeleccionado(){
        super("Color no seleccionado.\nSi desea personalizar el programa, seleccione un color");
    }
}