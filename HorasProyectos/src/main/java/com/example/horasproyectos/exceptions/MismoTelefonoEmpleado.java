package com.example.horasproyectos.exceptions;

public class MismoTelefonoEmpleado extends Exception{

    public MismoTelefonoEmpleado(){
        super("Error: el número de teléfono ya existe.\nPor favor, introduzca otro número.");
    }
}