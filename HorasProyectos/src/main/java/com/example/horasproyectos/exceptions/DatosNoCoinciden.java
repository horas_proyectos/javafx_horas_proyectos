package com.example.horasproyectos.exceptions;

public class DatosNoCoinciden extends Exception{

    public DatosNoCoinciden(){
        super("Los datos mostrados no coinciden.\nPor favor, pulse el botón de \"Validar\"");
    }
}