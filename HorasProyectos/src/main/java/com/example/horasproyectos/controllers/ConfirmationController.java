package com.example.horasproyectos.controllers;

import com.example.horasproyectos.InitialApplication;
import com.example.horasproyectos.exceptions.ErrorDeFK;
import com.example.horasproyectos.models.Empleado;
import com.example.horasproyectos.models.Participan;
import com.example.horasproyectos.models.Proyecto;
import com.example.horasproyectos.utilities.BridgeAPI;
import com.example.horasproyectos.utilities.Consultas;
import com.example.horasproyectos.utilities.CreaObjetos;
import javafx.fxml.FXML;
import javafx.stage.Stage;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

public class ConfirmationController {

    public Proyecto proyecto;
    public Participan participan;
    public Empleado empleado;

    public boolean error = false;

    public void recibeEmpleado(Empleado empleadoRecibido) {
        this.empleado = empleadoRecibido;
    }

    public void recibeProyecto(Proyecto proyectoRecibido) {
        this.proyecto = proyectoRecibido;
    }

    public void recibeParticipan(Participan participanRecibido) {
        this.participan = participanRecibido;
    }

    @FXML
    public void onConfirmationEliminarClick() throws IOException {

        if (this.empleado != null) {
            Empleado empleado = CreaObjetos.creaEmpleados(null, this.empleado);
            String participans = BridgeAPI.conection(Consultas.CONSULTA_LISTAR_PARTICIPAN);
            JSONArray jsonArray = new JSONArray(participans);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                if (jsonObject.get("empleadoid").equals(empleado.getEmpleadoid())) {
                    try {
                        throw new ErrorDeFK();
                    } catch (ErrorDeFK e) {
                        this.error = true;
                        InitialApplication.abrirStageInicial(new Stage(), "delete", e, empleado.getClass().toString(), true);
                        break;
                    }
                }
            }

            if (!this.error) {

                Consultas.deleteEmpleado(empleado);
                InitialApplication.abrirStageInicial(new Stage(), "delete", null, empleado.getClass().toString(), true);
            }
        }

        if (this.proyecto != null) {

            Proyecto proyecto = CreaObjetos.creaProyectos(null, this.proyecto);
            String participans = BridgeAPI.conection(Consultas.CONSULTA_LISTAR_PARTICIPAN);
            JSONArray jsonArray = new JSONArray(participans);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                if (jsonObject.get("proyectoid").equals(proyecto.getProyectoid())) {
                    try {
                        throw new ErrorDeFK();
                    } catch (ErrorDeFK e) {
                        this.error = true;
                        InitialApplication.abrirStageInicial(new Stage(), "delete", e, proyecto.getClass().toString(), true);
                        break;
                    }
                }
            }

            if (!this.error) {

                Consultas.deleteProyecto(proyecto);
                InitialApplication.abrirStageInicial(new Stage(), "delete", null, proyecto.getClass().toString(), true);
            }
        }

        if (this.participan != null) {

            Participan participan = CreaObjetos.creaParticipan(null, this.participan);
            Consultas.deleteParticipan(participan);

            InitialApplication.abrirStageInicial(new Stage(), "delete", null, participan.getClass().toString(), true);
        }
        onCancelarClick();
    }

    @FXML
    /**Este método cierra esta ventana actual*/
    public void onCancelarClick() {
        InitialApplication.stageConfirmacion.close();
    }

}
